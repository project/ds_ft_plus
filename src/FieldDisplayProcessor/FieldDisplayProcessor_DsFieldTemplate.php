<?php

namespace Drupal\ds_ft_plus\FieldDisplayProcessor;

use Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface;

class FieldDisplayProcessor_DsFieldTemplate implements FieldDisplayProcessorInterface {

  /**
   * @var array
   */
  private $dsExtrasFt;

  /**
   * @CfrPlugin("default", @t("Default field template setting"))
   *
   * @return \Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface
   */
  public static function createDefault() {
    return new self([]);
  }

  /**
   * @CfrPlugin("reset", @t("Full reset"))
   *
   * @return \Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface
   */
  public static function createReset() {
    $ft = ['func' => 'theme_ds_field_reset'];
    return new self($ft);
  }

  /**
   * @param array $dsExtrasFt
   */
  public function __construct(array $dsExtrasFt) {
    $this->dsExtrasFt = $dsExtrasFt;
  }

  /**
   * @param array $element
   *   Render array with ['#theme' => 'field', ..]
   *
   * @return array
   */
  public function process(array $element) {
    $element[DS_FT_PLUS_PROPERTY] = $this->dsExtrasFt;
    return $element;
  }
}
