<?php

namespace Drupal\ds_ft_plus\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\ds_ft_plus\FieldDisplayProcessor\FieldDisplayProcessor_DsFieldTemplate;

/**
 * @CfrPlugin("minimal", @t("Minimal"))
 */
class Configurator_FieldDisplayProcessor_Minimal implements ConfiguratorInterface {

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param string|null $label
   *   Label for the form element, specifying the purpose where it is used.
   *
   * @return array
   */
  public function confGetForm($conf, $label) {
    $form = [];

    $field_classes = _ds_classes('ds_classes_fields');

    // Field classes.
    if ([] !== $field_classes) {
      $form['classes'] = [
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $field_classes,
        '#title' => t('Choose additional CSS classes for the field'),
        // @todo This doesn't make sense, does it?
        '#default_value' => isset($field_settings['classes']) ? explode(' ', $field_settings['classes']) : [],
        '#prefix' => '<div class="field-classes">',
        '#suffix' => '</div>',
      ];
    }
    else {
      $form['classes'] = [
        '#type' => 'value',
        '#value' => [''],
      ];
    }

    $form['lb-col'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide label colon'),
      '#default_value' => isset($conf['lb-col']) ? $conf['lb-col'] : FALSE,
      '#attributes' => [
        'class' => ['colon-checkbox'],
      ],
    ];

    return $form;
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   An object that controls the format of the summary.
   *
   * @return mixed|string|null
   *   A string summary is always allowed. But other values may be returned if
   *   $summaryBuilder generates them.
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    // @todo Provide a summary.
    return NULL;
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   *
   * @return \Drupal\renderkit\FieldDisplayProcessor\FieldDisplayProcessorInterface
   *   Value to be used in the application.
   */
  public function confGetValue($conf) {
    $ft = $this->confGetFt($conf);
    return new FieldDisplayProcessor_DsFieldTemplate($ft);
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *
   * @return string
   *   PHP statement to generate the value.
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    $ft = $this->confGetFt($conf);

    $args_php = [] === $ft
      ? '[]'
      : "\n" . $helper->export($ft);

    return 'new ' . FieldDisplayProcessor_DsFieldTemplate::class . '(' . $args_php . ')';
  }

  /**
   * @param mixed $conf
   *
   * @return array
   */
  private function confGetFt($conf) {

    if (!is_array($conf)) {
      return [];
    }

    $ft = ['func' => 'theme_ds_field_minimal'];

    if (isset($conf['classes'])) {
      $ft['classes'] = is_array($conf['classes'])
        ? implode(' ', $conf['classes'])
        : (string)$conf['classes'];
    }

    if (!empty($conf['lb-col'])) {
      $ft['lb-col'] = TRUE;
    }

    return $ft;
  }
}
